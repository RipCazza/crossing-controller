import paho.mqtt.client as mqtt
import time
import threading


TEAM_ID = 7


class Light:
    def __init__(self, ids, group_id, user_type, sensors, congruent_lights):
        self.ids = ids
        self.group_id = group_id
        self.user_type = user_type
        self.sensors = sensors
        self.congruent_lights = congruent_lights

        self.active_sensors = []
        self.status = b'0'

        if self.user_type == 'motor_vehicle':
            self.green_time = 6
            self.orange_time = 4
            self.red_time = 1
        elif self.user_type == 'cycle':
            self.green_time = 8
            self.orange_time = 2
            self.red_time = 1
        elif self.user_type == 'foot':
            self.green_time = 6
            self.orange_time = 6
            self.red_time = 1
        elif self.user_type == 'vessel':
            self.green_time = 20
            self.orange_time = 0
            self.red_time = 10
        elif self.user_type == 'bridge':
            self.green_time = 600
            self.orange_time = 0
            self.red_time = 0

        self.timestamp = time.time()

    def matches_dict(self, dict_):
        return self.group_id == dict_['group_id'] and self.user_type == dict_['user_type']

    def update(self, client):
        self.status = b'2' if self.status == b'0' else b'1' if self.status == b'2' else b'0'
        self.timestamp = time.time()

        for id_ in self.ids:
            topic = '/'.join([str(TEAM_ID), self.user_type, str(self.group_id), "light",
                              str(id_)])
            client.publish(topic, payload=self.status, qos=1, retain=False)

    def find_congruent_lights(self, queue, currently_congruent=None):
        if currently_congruent is None:
            currently_congruent = self.congruent_lights

        if not currently_congruent:
            return []

        for light in queue:
            if _light_in_congruent_list(light, currently_congruent):
                # light IS CONGRUENT WITH EVERYTHING IN CURRENTLY CONGRUENT
                return [light] + light.find_congruent_lights(
                    queue, _intersection(currently_congruent, light.congruent_lights))
        return []


def _intersection(list_a, list_b):
    return [item for item in list_a if item in list_b]


def _light_in_congruent_list(light, congruent_list):
    for con_light in congruent_list:
        if light.matches_dict(con_light):
            return True
    return False


class Crossing:
    def __init__(self, lights, lights_queue, active_lights):
        self._lights = lights
        self.lights_queue = lights_queue
        self.active_lights = active_lights

    def get(self):
        return self._lights

    def update_queue(self, component):
        if component["payload"] == b'1':
            for light in self.lights_queue:
                if component["group_id"] == light.group_id and component["user_type"] == light.user_type and \
                        component["id"] in light.sensors:
                    if component["id"] not in light.active_sensors:
                        light.active_sensors.append(component["id"])
                    return

            for light in self.get():
                if component["group_id"] == light.group_id and component["user_type"] == light.user_type and \
                        component["id"] in light.sensors:
                    if component["id"] not in light.active_sensors:
                        light.active_sensors.append(component["id"])
                    self.lights_queue.append(light)

        elif component["payload"] == b'0':
            for light in self.lights_queue:
                if component["group_id"] == light.group_id and component["user_type"] == light.user_type and \
                        component["id"] in light.sensors:
                    if component["id"] in light.active_sensors:
                        light.active_sensors.remove(component["id"])
                    if len(light.active_sensors) == 0:
                        self.lights_queue.remove(light)
                    break

    def update_active(self, client):
        for light in self.active_lights:
            if light.status == b'2':
                if time.time() >= light.timestamp + light.green_time:
                    light.update(client)
            elif light.status == b'1':
                if time.time() >= light.timestamp + light.orange_time:
                    light.update(client)
            else:
                if light in self.active_lights and time.time() >= light.timestamp + light.red_time:
                    self.active_lights.remove(light)

    def add_active(self, light):
        if light not in self.active_lights:
            self.active_lights.append(light)

    def shuffle_lights(self, lights):
        """move lights to the back of the queue"""
        for light in lights:
            self.lights_queue.remove(light)
            self.lights_queue.append(light)

    def activate_congruent(self, client, congruent_lights):
        for congruent_light in congruent_lights:
            self.add_active(congruent_light)
        for active_light in self.active_lights:
            active_light.update(client)

    def reset(self, client, post):
        self.active_lights = []
        self.lights_queue = []
        for light in self._lights:
            light.status = b'0'
            light.timestamp = time.time()

        if post:
            for light in self._lights:
                for id_ in light.ids:
                    topic = '/'.join([str(TEAM_ID), light.user_type, str(light.group_id), "light",
                                      str(id_)])
                    client.publish(topic, payload=light.status, qos=1, retain=False)


class BridgeCrossing:
    def __init__(self):
        self.bridge_lights = to_component("7/bridge/1/light/1", b'2')
        self.gate_1 = to_component("7/bridge/1/gate/1", b'0')
        self.gate_2 = to_component("7/bridge/1/gate/2", b'0')
        self.bridge_deck = to_component("7/bridge/1/deck/1", b'1')
        self.vessel_light_1 = to_component("7/vessel/1/light/1", b'0')
        self.vessel_light_2 = to_component("7/vessel/2/light/1", b'0')
        self.vessel_sensors = {
            1: b'0',
            2: b'0',
            3: b'0',
        }
        self.vessel_timestamps = {
            1: time.time(),
            2: time.time(),
        }
        self.bridge_timestamp = time.time()
        self.bridge_sensor = b'0'
        self.opening = False

    def update_sensor(self, component):
        if component["user_type"] == "vessel":
            self.vessel_sensors[component["group_id"]] = component["payload"]
        elif component["user_type"] == "bridge":
            self.bridge_sensor[component["group_id"]] = component["payload"]

    def update(self, client, component, payload):
        component["payload"] = payload
        topic = '/'.join([str(TEAM_ID), component["user_type"], str(component["group_id"]), component["type"],
                          str(component["id"])])
        client.publish(topic, payload=component["payload"], qos=1, retain=False)


    def reset(self, client, post):
        self.bridge_lights = to_component("7/bridge/1/light/1", b'2')
        self.gate_1 = to_component("7/bridge/1/gate/1", b'0')
        self.gate_2 = to_component("7/bridge/1/gate/2", b'0')
        self.bridge_deck = to_component("7/bridge/1/deck/1", b'1')
        self.vessel_light_1 = to_component("7/vessel/1/light/1", b'0')
        self.vessel_light_2 = to_component("7/vessel/2/light/1", b'0')
        self.vessel_sensors = {
            1: b'0',
            2: b'0',
            3: b'0',
        }
        self.vessel_timestamps = {
            1: time.time(),
            2: time.time(),
        }
        self.bridge_timestamp = time.time()
        self.bridge_sensor = b'0'
        self.opening = False

        if post:
            for component in [self.bridge_lights, self.gate_1, self.gate_2, self.bridge_deck,
                              self.vessel_light_1, self.vessel_light_2]:
                topic = '/'.join([str(TEAM_ID), component["user_type"], str(component["group_id"]), component["type"],
                                  str(component["id"])])
                client.publish(topic, payload=component["payload"], qos=1, retain=False)


class MyClient(mqtt.Client):

    def __init__(self, crossing, bridge_crossing):
        super().__init__()
        self.crossing = crossing
        self.bridge_crossing = bridge_crossing

        self.on_connect = on_connect
        self.on_disconnect = on_disconnect
        self.on_message = on_message
        self.on_publish = on_publish


class CrossingThread(threading.Thread):
    def __init__(self, crossing, client):
        super().__init__()
        self.crossing = crossing
        self.client = client

        self._stop_event = threading.Event()
        self.daemon = True

    def run(self):
        while True:
            time.sleep(0.1)

            # Activate new lights
            if len(self.crossing.active_lights) == 0 and len(self.crossing.lights_queue) != 0:
                top_light = self.crossing.lights_queue[0]
                congruent_lights = top_light.find_congruent_lights(self.crossing.lights_queue)
                congruent_lights = [top_light] + congruent_lights
                self.crossing.activate_congruent(self.client, congruent_lights)

                self.crossing.shuffle_lights(congruent_lights)

            self.crossing.update_active(self.client)


class BridgeCrossingThread(threading.Thread):
    def __init__(self, crossing, client):
        super().__init__()
        self.crossing = crossing
        self.client = client

        self._stop_event = threading.Event()
        self.daemon = True

    def run(self):
        while True:
            time.sleep(0.1)
            # bridge is closed
            if self.crossing.bridge_deck["payload"] == b'1':
                # road lights are red
                if self.crossing.bridge_lights["payload"] == b'0':
                    # opening sequence
                    if self.crossing.opening:
                        # close first gates
                        if self.crossing.gate_1["payload"] == b'0' and time.time() >= self.crossing.bridge_timestamp + 6:
                            self.crossing.update(self.client, self.crossing.gate_1, b'1')
                        elif self.crossing.gate_1["payload"] == b'1':
                            # close second gates
                            if self.crossing.gate_2["payload"] == b'0' and self.crossing.bridge_sensor == b'0':
                                self.crossing.update(self.client, self.crossing.gate_2, b'1')
                                self.crossing.bridge_timestamp = time.time()
                            # open bridge
                            elif self.crossing.gate_2["payload"] == b'1' and self.crossing.bridge_sensor == b'0' and \
                                    time.time() >= self.crossing.bridge_timestamp + 4:
                                self.crossing.update(self.client, self.crossing.bridge_deck, b'0')
                                self.crossing.bridge_timestamp = time.time()
                    # closing sequence
                    else:
                        # open gates
                        if self.crossing.gate_1["payload"] == b'1' and time.time() >= self.crossing.bridge_timestamp + 10:
                            self.crossing.update(self.client, self.crossing.gate_1, b'0')
                            self.crossing.update(self.client, self.crossing.gate_2, b'0')
                            self.crossing.bridge_timestamp = time.time()
                        # set road lights green
                        elif self.crossing.gate_1["payload"] == b'0' and self.crossing.bridge_timestamp + 4:
                            self.crossing.update(self.client, self.crossing.bridge_lights, b'2')
                            self.crossing.bridge_timestamp = time.time()
                # watch vessels
                elif self.crossing.bridge_lights["payload"] == b'2' and time.time() >= self.crossing.bridge_timestamp + 20:
                    for sensor in self.crossing.vessel_sensors.values():
                        # set road lights to red
                        if sensor == b'1':
                            self.crossing.update(self.client, self.crossing.bridge_lights, b'0')
                            self.crossing.bridge_timestamp = time.time()
                            self.crossing.opening = True
                            break

            # bridge is open
            elif self.crossing.bridge_deck["payload"] == b'0':
                # opening sequence
                if self.crossing.opening:
                    # bridge opening
                    if self.crossing.vessel_light_1["payload"] == b'0' and self.crossing.vessel_light_2["payload"] == b'0' and \
                            time.time() >= self.crossing.bridge_timestamp + 10:
                        self.crossing.update(self.client, self.crossing.vessel_light_1, b'2')
                    # north vessels crossing
                    elif self.crossing.vessel_light_1["payload"] == b'2':
                        # no more vessels
                        if self.crossing.vessel_sensors[1] == b'0' and self.crossing.vessel_sensors[3] == b'0':
                            self.crossing.update(self.client, self.crossing.vessel_light_1, b'0')
                            self.crossing.update(self.client, self.crossing.vessel_light_2, b'2')
                            self.crossing.bridge_timestamp = time.time()
                    # south vessels crossing
                    elif self.crossing.vessel_light_2["payload"] == b'2':
                        # no more vessels
                        if self.crossing.vessel_sensors[2] == b'0':
                            self.crossing.update(self.client, self.crossing.vessel_light_2, b'0')
                            self.crossing.bridge_timestamp = time.time()
                            self.crossing.opening = False
                # closing sequence
                else:
                    # close
                    if self.crossing.vessel_sensors[3] == b'0':
                        self.crossing.update(self.client, self.crossing.bridge_deck, b'1')
                        self.crossing.bridge_timestamp = time.time()


def to_component(component_string, payload):
    components = component_string.split("/")
    component_dict = {
        "team": int(components[0]),
        "user_type": components[1],
        "group_id": int(components[2]),
        "type": components[3],
        "id": int(components[4]),
        "payload": payload
    }
    return component_dict


def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    client.subscribe(str(TEAM_ID) + "/+/+/sensor/+")
    client.subscribe(str(TEAM_ID) + "/features/lifecycle/simulator/+")

    onconnect_topic = '/'.join([str(TEAM_ID), "features", "lifecycle", "controller", "onconnect"])
    client.publish(onconnect_topic, payload=None, qos=1, retain=False)

    # TESTING STUFF
    # time.sleep(1)
    # client.publish(str(TEAM_ID) + "/motor_vehicle/1/sensor/1", payload=b'1', qos=1, retain=False)
    # client.publish(str(TEAM_ID) + "/motor_vehicle/2/sensor/1", payload=b'1', qos=1, retain=False)


def on_disconnect(client, userdata, rc):
    topic = '/'.join([str(TEAM_ID), "features", "lifecycle", "controller", "ondisconnect"])
    client.publish(topic, payload=None, qos=1, retain=False)


def on_message(client, userdata, msg):
    # print(msg.topic + " " + str(msg.payload))

    if msg.topic.startswith(str(TEAM_ID) + "/features/lifecycle/simulator/"):
        if msg.topic.endswith("onconnect"):
            client.crossing.reset(client, True)
            client.bridge_crossing.reset(client, True)
        else:
            client.crossing.reset(client, False)
            client.bridge_crossing.reset(client, False)
        return

    component = to_component(msg.topic, msg.payload)

    if component["type"] == "sensor":
        if is_bridge_crossing(component):
            client.bridge_crossing.update_sensor(component)
        else:
            client.crossing.update_queue(component)


def on_publish(client, userdata, mid):
    pass


def is_bridge_crossing(component):
    if component["user_type"] == "bridge" or component["user_type"] == "vessel":
        return True
    return False


def main():
    lights = [
        Light([1], 1, "motor_vehicle", [1], [
            {
                "user_type": "motor_vehicle",
                "group_id": 2,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 3,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 4,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 6,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 7,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 9,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 10,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 11,
            },
            {
                "user_type": "cycle",
                "group_id": 2,
            },
            {
                "user_type": "cycle",
                "group_id": 3,
            },
            {
                "user_type": "foot",
                "group_id": 3,
            },
            {
                "user_type": "foot",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 5,
            },
            {
                "user_type": "foot",
                "group_id": 6,
            },
        ]),
        Light([1], 2, "motor_vehicle", [1], [
            {
                "user_type": "motor_vehicle",
                "group_id": 1,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 3,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 4,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 7,
            },
            {
                "user_type": "cycle",
                "group_id": 2,
            },
            {
                "user_type": "cycle",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 3,
            },
            {
                "user_type": "foot",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 7,
            },
            {
                "user_type": "foot",
                "group_id": 8,
            },
        ]),
        Light([1], 3, "motor_vehicle", [1], [
            {
                "user_type": "motor_vehicle",
                "group_id": 1,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 2,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 4,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 9,
            },
            {
                "user_type": "cycle",
                "group_id": 3,
            },
            {
                "user_type": "cycle",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 5,
            },
            {
                "user_type": "foot",
                "group_id": 6,
            },
            {
                "user_type": "foot",
                "group_id": 7,
            },
            {
                "user_type": "foot",
                "group_id": 8,
            },
        ]),
        Light([1], 4, "motor_vehicle", [1], [
            {
                "user_type": "motor_vehicle",
                "group_id": 5,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 6,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 7,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 9,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 10,
            },
            {
                "user_type": "cycle",
                "group_id": 3,
            },
            {
                "user_type": "cycle",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 5,
            },
            {
                "user_type": "foot",
                "group_id": 6,
            },
            {
                "user_type": "foot",
                "group_id": 7,
            },
            {
                "user_type": "foot",
                "group_id": 8,
            },
        ]),
        Light([1, 2], 5, "motor_vehicle", [1, 4], [
            {
                "user_type": "motor_vehicle",
                "group_id": 4,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 6,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 7,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 9,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 10,
            },
            {
                "user_type": "cycle",
                "group_id": 1,
            },
            {
                "user_type": "cycle",
                "group_id": 3,
            },
            {
                "user_type": "foot",
                "group_id": 1,
            },
            {
                "user_type": "foot",
                "group_id": 2,
            },
            {
                "user_type": "foot",
                "group_id": 5,
            },
            {
                "user_type": "foot",
                "group_id": 6,
            },
        ]),
        Light([1], 6, "motor_vehicle", [1], [
            {
                "user_type": "motor_vehicle",
                "group_id": 1,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 4,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 5,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 7,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 11,
            },
            {
                "user_type": "cycle",
                "group_id": 1,
            },
            {
                "user_type": "cycle",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 1,
            },
            {
                "user_type": "foot",
                "group_id": 2,
            },
            {
                "user_type": "foot",
                "group_id": 7,
            },
            {
                "user_type": "foot",
                "group_id": 8,
            },
        ]),
        Light([1, 2], 7, "motor_vehicle", [1, 3], [
            {
                "user_type": "motor_vehicle",
                "group_id": 1,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 2,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 4,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 5,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 6,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 8,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 9,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 11,
            },
            {
                "user_type": "cycle",
                "group_id": 1,
            },
            {
                "user_type": "cycle",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 1,
            },
            {
                "user_type": "foot",
                "group_id": 2,
            },
            {
                "user_type": "foot",
                "group_id": 7,
            },
            {
                "user_type": "foot",
                "group_id": 8,
            },
        ]),
        Light([1], 8, "motor_vehicle", [1], [
            {
                "user_type": "motor_vehicle",
                "group_id": 7,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 9,
            },
            {
                "user_type": "cycle",
                "group_id": 2,
            },
            {
                "user_type": "foot",
                "group_id": 3,
            },
            {
                "user_type": "foot",
                "group_id": 4,
            },
        ]),
        Light([1], 9, "motor_vehicle", [1], [
            {
                "user_type": "motor_vehicle",
                "group_id": 1,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 3,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 4,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 5,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 7,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 8,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 10,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 11,
            },
            {
                "user_type": "cycle",
                "group_id": 1,
            },
            {
                "user_type": "cycle",
                "group_id": 2,
            },
            {
                "user_type": "foot",
                "group_id": 1,
            },
            {
                "user_type": "foot",
                "group_id": 2,
            },
            {
                "user_type": "foot",
                "group_id": 3,
            },
            {
                "user_type": "foot",
                "group_id": 4,
            },
        ]),
        Light([1, 2], 10, "motor_vehicle", [1, 4], [
            {
                "user_type": "motor_vehicle",
                "group_id": 1,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 4,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 5,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 9,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 11,
            },
            {
                "user_type": "cycle",
                "group_id": 1,
            },
            {
                "user_type": "cycle",
                "group_id": 3,
            },
            {
                "user_type": "foot",
                "group_id": 1,
            },
            {
                "user_type": "foot",
                "group_id": 2,
            },
            {
                "user_type": "foot",
                "group_id": 5,
            },
            {
                "user_type": "foot",
                "group_id": 6,
            },
        ]),
        Light([1], 11, "motor_vehicle", [1], [
            {
                "user_type": "motor_vehicle",
                "group_id": 1,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 6,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 7,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 9,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 10,
            },
            {
                "user_type": "cycle",
                "group_id": 2,
            },
            {
                "user_type": "cycle",
                "group_id": 3,
            },
            {
                "user_type": "foot",
                "group_id": 3,
            },
            {
                "user_type": "foot",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 5,
            },
            {
                "user_type": "foot",
                "group_id": 6,
            },
        ]),
        Light([1], 12, "motor_vehicle", [1], [
            {
                "user_type": "motor_vehicle",
                "group_id": 1,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 3,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 4,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 5,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 10,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 11,
            },
            {
                "user_type": "cycle",
                "group_id": 1,
            },
            {
                "user_type": "cycle",
                "group_id": 2,
            },
            {
                "user_type": "cycle",
                "group_id": 3,
            },
            {
                "user_type": "cycle",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 1,
            },
            {
                "user_type": "foot",
                "group_id": 2,
            },
            {
                "user_type": "foot",
                "group_id": 3,
            },
            {
                "user_type": "foot",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 5,
            },
            {
                "user_type": "foot",
                "group_id": 6,
            },
            {
                "user_type": "foot",
                "group_id": 7,
            },
            {
                "user_type": "foot",
                "group_id": 8,
            },
        ]),
        Light([1], 1, "cycle", [1], [
            {
                "user_type": "motor_vehicle",
                "group_id": 5,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 6,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 7,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 9,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 10,
            },
            {
                "user_type": "cycle",
                "group_id": 2,
            },
            {
                "user_type": "cycle",
                "group_id": 3,
            },
            {
                "user_type": "cycle",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 1,
            },
            {
                "user_type": "foot",
                "group_id": 2,
            },
            {
                "user_type": "foot",
                "group_id": 3,
            },
            {
                "user_type": "foot",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 5,
            },
            {
                "user_type": "foot",
                "group_id": 6,
            },
            {
                "user_type": "foot",
                "group_id": 7,
            },
            {
                "user_type": "foot",
                "group_id": 8,
            },
        ]),
        Light([1], 2, "cycle", [1], [
            {
                "user_type": "motor_vehicle",
                "group_id": 1,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 2,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 8,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 9,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 11,
            },
            {
                "user_type": "cycle",
                "group_id": 1,
            },
            {
                "user_type": "cycle",
                "group_id": 3,
            },
            {
                "user_type": "cycle",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 1,
            },
            {
                "user_type": "foot",
                "group_id": 2,
            },
            {
                "user_type": "foot",
                "group_id": 3,
            },
            {
                "user_type": "foot",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 5,
            },
            {
                "user_type": "foot",
                "group_id": 6,
            },
            {
                "user_type": "foot",
                "group_id": 7,
            },
            {
                "user_type": "foot",
                "group_id": 8,
            },
        ]),
        Light([1], 3, "cycle", [1], [
            {
                "user_type": "motor_vehicle",
                "group_id": 1,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 3,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 4,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 5,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 10,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 11,
            },
            {
                "user_type": "cycle",
                "group_id": 1,
            },
            {
                "user_type": "cycle",
                "group_id": 2,
            },
            {
                "user_type": "cycle",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 1,
            },
            {
                "user_type": "foot",
                "group_id": 2,
            },
            {
                "user_type": "foot",
                "group_id": 3,
            },
            {
                "user_type": "foot",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 5,
            },
            {
                "user_type": "foot",
                "group_id": 6,
            },
            {
                "user_type": "foot",
                "group_id": 7,
            },
            {
                "user_type": "foot",
                "group_id": 8,
            },
        ]),
        Light([1], 4, "cycle", [1], [
            {
                "user_type": "motor_vehicle",
                "group_id": 2,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 3,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 4,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 6,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 7,
            },
            {
                "user_type": "cycle",
                "group_id": 1,
            },
            {
                "user_type": "cycle",
                "group_id": 2,
            },
            {
                "user_type": "cycle",
                "group_id": 3,
            },
            {
                "user_type": "foot",
                "group_id": 1,
            },
            {
                "user_type": "foot",
                "group_id": 2,
            },
            {
                "user_type": "foot",
                "group_id": 3,
            },
            {
                "user_type": "foot",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 5,
            },
            {
                "user_type": "foot",
                "group_id": 6,
            },
            {
                "user_type": "foot",
                "group_id": 7,
            },
            {
                "user_type": "foot",
                "group_id": 8,
            },
        ]),
        Light([1, 2], 1, "foot", [1, 2], [
            {
                "user_type": "motor_vehicle",
                "group_id": 5,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 6,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 7,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 9,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 10,
            },
            {
                "user_type": "cycle",
                "group_id": 1,
            },
            {
                "user_type": "cycle",
                "group_id": 2,
            },
            {
                "user_type": "cycle",
                "group_id": 3,
            },
            {
                "user_type": "cycle",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 2,
            },
            {
                "user_type": "foot",
                "group_id": 3,
            },
            {
                "user_type": "foot",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 5,
            },
            {
                "user_type": "foot",
                "group_id": 6,
            },
            {
                "user_type": "foot",
                "group_id": 7,
            },
            {
                "user_type": "foot",
                "group_id": 8,
            },
        ]),
        Light([1, 2], 2, "foot", [1, 2], [
            {
                "user_type": "motor_vehicle",
                "group_id": 5,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 6,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 7,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 9,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 10,
            },
            {
                "user_type": "cycle",
                "group_id": 1,
            },
            {
                "user_type": "cycle",
                "group_id": 2,
            },
            {
                "user_type": "cycle",
                "group_id": 3,
            },
            {
                "user_type": "cycle",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 1,
            },
            {
                "user_type": "foot",
                "group_id": 3,
            },
            {
                "user_type": "foot",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 5,
            },
            {
                "user_type": "foot",
                "group_id": 6,
            },
            {
                "user_type": "foot",
                "group_id": 7,
            },
            {
                "user_type": "foot",
                "group_id": 8,
            },
        ]),
        Light([1, 2], 3, "foot", [1, 2], [
            {
                "user_type": "motor_vehicle",
                "group_id": 1,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 2,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 8,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 9,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 11,
            },
            {
                "user_type": "cycle",
                "group_id": 1,
            },
            {
                "user_type": "cycle",
                "group_id": 2,
            },
            {
                "user_type": "cycle",
                "group_id": 3,
            },
            {
                "user_type": "cycle",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 1,
            },
            {
                "user_type": "foot",
                "group_id": 2,
            },
            {
                "user_type": "foot",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 5,
            },
            {
                "user_type": "foot",
                "group_id": 6,
            },
            {
                "user_type": "foot",
                "group_id": 7,
            },
            {
                "user_type": "foot",
                "group_id": 8,
            },
        ]),
        Light([1, 2], 4, "foot", [1, 2], [
            {
                "user_type": "motor_vehicle",
                "group_id": 1,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 2,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 8,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 9,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 11,
            },
            {
                "user_type": "cycle",
                "group_id": 1,
            },
            {
                "user_type": "cycle",
                "group_id": 2,
            },
            {
                "user_type": "cycle",
                "group_id": 3,
            },
            {
                "user_type": "cycle",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 1,
            },
            {
                "user_type": "foot",
                "group_id": 2,
            },
            {
                "user_type": "foot",
                "group_id": 3,
            },
            {
                "user_type": "foot",
                "group_id": 5,
            },
            {
                "user_type": "foot",
                "group_id": 6,
            },
            {
                "user_type": "foot",
                "group_id": 7,
            },
            {
                "user_type": "foot",
                "group_id": 8,
            },
        ]),
        Light([1, 2], 5, "foot", [1, 2], [
            {
                "user_type": "motor_vehicle",
                "group_id": 1,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 3,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 4,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 5,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 10,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 11,
            },
            {
                "user_type": "cycle",
                "group_id": 1,
            },
            {
                "user_type": "cycle",
                "group_id": 2,
            },
            {
                "user_type": "cycle",
                "group_id": 3,
            },
            {
                "user_type": "cycle",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 1,
            },
            {
                "user_type": "foot",
                "group_id": 2,
            },
            {
                "user_type": "foot",
                "group_id": 3,
            },
            {
                "user_type": "foot",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 6,
            },
            {
                "user_type": "foot",
                "group_id": 7,
            },
            {
                "user_type": "foot",
                "group_id": 8,
            },
        ]),
        Light([1, 2], 6, "foot", [1, 2], [
            {
                "user_type": "motor_vehicle",
                "group_id": 1,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 3,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 4,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 5,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 10,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 11,
            },
            {
                "user_type": "cycle",
                "group_id": 1,
            },
            {
                "user_type": "cycle",
                "group_id": 2,
            },
            {
                "user_type": "cycle",
                "group_id": 3,
            },
            {
                "user_type": "cycle",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 1,
            },
            {
                "user_type": "foot",
                "group_id": 2,
            },
            {
                "user_type": "foot",
                "group_id": 3,
            },
            {
                "user_type": "foot",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 5,
            },
            {
                "user_type": "foot",
                "group_id": 7,
            },
            {
                "user_type": "foot",
                "group_id": 8,
            },
        ]),
        Light([1, 2], 7, "foot", [1, 2], [
            {
                "user_type": "motor_vehicle",
                "group_id": 2,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 3,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 4,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 6,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 7,
            },
            {
                "user_type": "cycle",
                "group_id": 1,
            },
            {
                "user_type": "cycle",
                "group_id": 2,
            },
            {
                "user_type": "cycle",
                "group_id": 3,
            },
            {
                "user_type": "cycle",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 1,
            },
            {
                "user_type": "foot",
                "group_id": 2,
            },
            {
                "user_type": "foot",
                "group_id": 3,
            },
            {
                "user_type": "foot",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 5,
            },
            {
                "user_type": "foot",
                "group_id": 6,
            },
            {
                "user_type": "foot",
                "group_id": 8,
            },
        ]),
        Light([1, 2], 8, "foot", [1, 2], [
            {
                "user_type": "motor_vehicle",
                "group_id": 2,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 3,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 4,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 6,
            },
            {
                "user_type": "motor_vehicle",
                "group_id": 7,
            },
            {
                "user_type": "cycle",
                "group_id": 1,
            },
            {
                "user_type": "cycle",
                "group_id": 2,
            },
            {
                "user_type": "cycle",
                "group_id": 3,
            },
            {
                "user_type": "cycle",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 1,
            },
            {
                "user_type": "foot",
                "group_id": 2,
            },
            {
                "user_type": "foot",
                "group_id": 3,
            },
            {
                "user_type": "foot",
                "group_id": 4,
            },
            {
                "user_type": "foot",
                "group_id": 5,
            },
            {
                "user_type": "foot",
                "group_id": 6,
            },
            {
                "user_type": "foot",
                "group_id": 7,
            },
        ]),
    ]

    crossing = Crossing(
        lights,
        [],
        []
    )

    bridge_crossing = BridgeCrossing()

    client = MyClient(crossing, bridge_crossing)

    thread = CrossingThread(crossing, client)
    thread.start()

    bridge_thread = BridgeCrossingThread(bridge_crossing, client)
    bridge_thread.start()

    will_topic = '/'.join([str(TEAM_ID), "features", "lifecycle", "controller", "ondisconnect"])
    client.will_set(will_topic, payload=b'1', qos=1, retain=True)

    client.connect("broker.0f.nl", 1883, 60)

    print("Start loop_forever")
    client.loop_forever()


if __name__ == "__main__":
    main()
